//25.09 домашка. Задача 3, вычислить определитель матрицы
import java.util.Scanner;
import java.util.Random;

public class Zadacha3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("введите размер матрицы");
        System.out.println("1. 2x2\n2. 3x3");
        int num = in.nextInt();
        switch (num)
        {
            case 1:
                int[][] a1 = new int[2][2];
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        a1 [i][j] = rand.nextInt(10);
                    }
                }
                System.out.println("Матрица");
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        System.out.print( a1 [i][j] + "\t");
                }
                    System.out.println();
            }
               int m1 = a1 [0][0] * a1 [1][1] - a1[0][1] * a1[1][0];
                System.out.println(m1);
                break;
            case 2:
                int[][] a2 = new int[3][3];
                int m2 = 0;
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        a2[i][j] = rand.nextInt(10);
                    }
                }
                System.out.println("Матрица");
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        System.out.print( a2 [i][j] + "\t");
                    }
                    System.out.println();
                }
                    m2 = (a2[0][0] * a2[1][1] * a2[2][2] + a2[0][1] * a2[1][2] * a2[2][0] +
                            a2[0][1] * a2[1][2] * a2[0][2]) - (a2[0][2] * a2[1][1] * a2[2][0] +
                            a2[1][0] * a2[0][1] * a2[2][2] + a2[0][0] * a2[1][2] * a2[2][1]);
                System.out.println(m2);
                break;
            default:
                return;
        }
    }
}
