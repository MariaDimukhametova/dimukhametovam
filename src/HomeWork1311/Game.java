package HomeWork1311;
import java.util.Scanner;
public class Game {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите имя первого игрока: ");
        String name1 = in.nextLine();
        System.out.print("Введите имя второго игрока: ");
        String name2 = in.nextLine();
        System.out.print("Введите количество очков здоровья для каждого игрока: ");
        int numberOfHP = in.nextInt();
        Player player1 = new Player(name1, numberOfHP);
        Player player2 = new Player(name2, numberOfHP);
        int power;

        while (player1.getHealthPoints() > 0 && player2.getHealthPoints() > 0) {
            System.out.println("Ход игрока 1:");
            System.out.print("Введите значение силы (от 1 до 9): ");
            power = in.nextInt();
                while ( power <=0 || power > 9) {
                    System.out.println("Введено неправильное значение. Попробуйте еще раз: ");
                    power = in.nextInt();
                }

            player2.hit(power);

            if (player2.getHealthPoints() <= 0) {
                System.out.println("Игрок 1 победил!");
                break;
            }

            System.out.println("Ход игрока 2:");
            System.out.print("Введите значение силы (от 1 до 9): ");
            power = in.nextInt();
            while ( power <=0 || power > 9) {
                System.out.print("Введено неправильное значение. Попробуйте еще раз: ");
                power = in.nextInt();
            }

            player1.hit(power);

            if (player1.getHealthPoints() <= 0) {
                System.out.println("Игрок 2 победил!");
                break;
            }
        }
    }
}
