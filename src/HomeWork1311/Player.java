package HomeWork1311;
import java.util.Random;

public class Player {
    Random rand = new Random();
    private String name;
    private int healthPoints;

    public Player (String name, int healthPoints) {
        this.name = name;
        this.healthPoints = healthPoints;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    void hit (int power) {
        int powerToPercent = (power + 1) * 10;
        int random = rand.nextInt(100) + 1;
        if (powerToPercent < random) {
            this.healthPoints -= power;
        } else {
            System.out.println("Промах!");
            this.healthPoints -= 0;
        }

    }
}
