//25.09. классная. Транспонирование матрицы

import java.util.Scanner;

    public class a4 {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int n = in.nextInt();
            int m = in.nextInt();
            int[][] a = new int[n][m];
            System.out.println("Матрица 1");
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a[i].length; j++) {
                    System.out.println(a[i][j]);
                }
            }

            for (int i = 0; i < a.length; i++) {
                for (int j = i + 1; j < a[i].length; j++) {
                    int A = a[i][j];
                    a[i][j] = a[j][i];
                    a[j][i] = A;
                }
            }

            System.out.println("Матрица 2");

            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a[i].length; j++) {
                    System.out.println(a[i][j]);
                }
            }
        }
    }

