import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a = in.nextDouble();
        double b = in.nextDouble();
        double c = in.nextDouble();
        double d = in.nextDouble();
        Vector2D vector1 = new Vector2D(a, b);
        Vector2D vector2 = new Vector2D(c, d);
        vector1.add(vector2);
        vector1.sub(vector2);
        double n = in.nextDouble();
        vector1.mult(n);
        vector2.mult(n);
    }
}
