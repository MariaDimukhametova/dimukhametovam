//методы
import java.util.Scanner;
import java.util.Random;

public class Methods {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Введите строку:");
        String str = in.nextLine();
        System.out.println("Строка в обратном порядке:");
        reverse(str);
        System.out.println("Введите число:");
        int x = in.nextInt();
        System.out.println("Количество разрядов числа:");
        rang(x);
        System.out.println("Введите длину массива:");
        int n = in.nextInt();
        int[] ar = new int[n];
        for (int i = 0; i < ar.length; i++) {
            ar[i] = rand.nextInt(20);
        }
        System.out.println("Массив:");
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i] + " ");
        }
        System.out.println("");
        System.out.println("Максимальное значение массива:");
        maxArray(ar);
        System.out.println("Введите длину первого массива:");
        int n1 = in.nextInt();
        int[] ar1 = new int[n1];
        for (int i = 0; i < ar1.length; i++) {
            ar1[i] = rand.nextInt(20);
        }
        System.out.println("Массив 1:");
        for (int i = 0; i < ar1.length; i++) {
            System.out.print(ar1[i] + " ");
        }
        System.out.println("");
        System.out.println("Введите длину второго массива:");
        int n2 = in.nextInt();
        int[] ar2 = new int[n2];
        for (int i = 0; i < ar2.length; i++) {
            ar2[i] = rand.nextInt(20);
        }
        System.out.println("Массив 2:");
        for (int i = 0; i < ar2.length; i++) {
            System.out.print(ar2[i] + " ");
        }
        System.out.println("");
        System.out.println("Полученный массив:");
        array2(ar1, ar2);
        System.out.println();
        System.out.println("Введите количество столбцов:");
        int n41 = in.nextInt();
        System.out.println("Введите количество строк:");
        int n42 = in.nextInt();
        int[][] ar4 = new int[n41][n42];
        for (int i = 0; i < ar4.length; i++) {
            for (int j = 0; j < ar4[i].length; j++) {
                ar4[i][j] = rand.nextInt(20);
            }
        }
        System.out.println("Матрица:");
        for (int i = 0; i < ar4.length; i++) {
            for (int j = 0; j < ar4.length; j++) {
                System.out.print(ar4[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("Транспонированная матрица:");
        matrixTranspose(ar4);
        System.out.println("Введите количество столбцов:");
        int n61 = in.nextInt();
        System.out.println("Введите количество строк:");
        int n62 = in.nextInt();
        int[][] ar6 = new int[n61][n62];
        for (int i = 0; i < ar6.length; i++) {
            for (int j = 0; j < ar6[i].length; j++) {
                ar6[i][j] = rand.nextInt(20);
            }
        }
        System.out.println("Матрица 1:");
        for (int i = 0; i < ar6.length; i++) {
            for (int j = 0; j < ar6[i].length; j++) {
                System.out.print(ar6[i][j] + "\t");
            }
            System.out.println();
        }
        int[][] ar7 = new int[n61][n62];
        for (int i = 0; i < ar7.length; i++) {
            for (int j = 0; j < ar7[i].length; j++) {
                ar7[i][j] = rand.nextInt(20);
            }
        }
        System.out.println("Матрица 2:");
        for (int i = 0; i < ar7.length; i++) {
            for (int j = 0; j < ar7[i].length; j++) {
                System.out.print(ar7[i] + "\t");
            }
            System.out.println();
        }
        System.out.println("Сумма матриц:");
        matrixSum(ar6, ar7);
    }
    public static void rang(int x) {
        int r = 0;
        while (x > 0) {
            x = x / 10;
            r++;
        }
        System.out.println(r);
    }
    public static void reverse(String str) {
        char[] ar = new char[str.length()];
        for (int i = 0; i < ar.length; i++) {
            ar[i] = str.charAt(i);
        }
        char[] ch = new char[str.length()];
        for (int i = 0; i < ar.length; i++) {
            ch[ar.length - 1 - i] = ar[i];
        }
        str = String.valueOf(ch);
        System.out.println(str);
    }
    public static void maxArray(int[] ar) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] > max) {
                max = ar[i];
            }
        }
        System.out.println(max);
    }
    public static void array2(int[] ar1, int[] ar2) {
        int[] ar3 = new int[ar1.length + ar2.length];
        for (int i = 0; i < ar3.length; i++) {
            if (i < ar1.length) {
                ar3[i] = ar1[i];
            } else {
                ar3[i] = ar2[i - ar1.length];
            }
        }
        for (int i = 0; i < ar3.length; i++) {
            System.out.print(ar3[i] + " ");
        }
    }
    public static void matrixTranspose(int[][] ar4) {
        int[][] ar5 = new int[ar4.length][ar4[0].length];
        for (int i = 0; i < ar5.length; i++) {
            for (int j = 0; j < ar5.length; j++) {
                ar5[i][j] = ar4[j][i];
            }
        }
        System.out.println("\n");
        for (int i = 0; i < ar5.length; i++) {
            for (int j = 0; j < ar5[i].length; j++) {
                System.out.print(ar5[i][j] + "\t");
            }
            System.out.println();
        }
    }
    public static void matrixSum(int[][] ar6, int[][] ar7) {
        int ar8[][] = new int[ar6.length][ar6[0].length];
        for (int i = 0; i < ar8.length; i++) {
            for (int j = 0; j < ar8[i].length; j++) {
                ar8[i][j] = ar6[i][j] + ar7[i][j];
            }
        }
        for (int i = 0; i < ar8.length; i++) {
            for (int j = 0; j < ar8[i].length; j++) {
                System.out.print(ar8[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

