public class Vector2D {
    private double x;
    private double y;

    public Vector2D(double x0, double y0) {
        this.x = x0;
        this.y = y0;
    }

    public void add(Vector2D other) {
        this.x += other.x;
        this.y += other.y;
        System.out.println("Сумма: "+ this.x + ", " + this.y);

    }

    public void sub(Vector2D other) {
        this.x -= other.x;
        this.y -= other.y;
        System.out.println("Разность: " + this.x + "," + this.y);
    }

    public void mult(double t) {
        this.x *= t;
        this.y *= t;
        System.out.println("Умножение на число: " + this.x + "," + this.y);
    }
}



