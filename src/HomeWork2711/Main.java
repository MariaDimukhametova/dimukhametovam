package HomeWork2711;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Shape[] shapeArea = new Shape[6];
        System.out.print("Введите длину и ширину 1 прямоугольника: ");
        shapeArea[0] = new Rectangle(sc.nextInt(), sc.nextInt());
        System.out.println("Введите длину и ширину 2 прямоугольника: ");
        shapeArea[1] = new Rectangle(sc.nextInt(), sc.nextInt());
        System.out.print("Введите радиус 1 круга: ");
        shapeArea[2] = new Circle(sc.nextInt());
        System.out.print("Введите радиус 2 круга: ");
        shapeArea[3] = new Circle(sc.nextInt());
        System.out.println("Введите длину и высоту 1 треугольника: ");
        shapeArea[4] = new Triangle(sc.nextInt(), sc.nextInt());
        System.out.println("Введите длину и высоту 2 треугольника: ");
        shapeArea[5] = new Triangle(sc.nextInt(), sc.nextInt());


        for (int i = 0; i < shapeArea.length; i++) {
            if (i % 2 == 0) {
                System.out.print("1st ");
                shapeArea[i].getArea();
            } else {
                System.out.print("2nd ");
                shapeArea[i].getArea();
            }
        }
    }
}