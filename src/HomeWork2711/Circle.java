package HomeWork2711;
public class Circle extends Shape {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public void getArea() {
        System.out.println("Circle's area: " + Math.pow(radius, 2) * Math.PI);
    }
}
