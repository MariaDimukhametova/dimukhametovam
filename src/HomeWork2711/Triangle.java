package HomeWork2711;
public class Triangle extends Shape {
    private double length;
    private double height;

    public Triangle (int length, int height) {
        this.length = length;
        this.height = height;
    }

    @Override
    public void getArea() {
        System.out.println("Triangle's area: " + length * height / 2);
    }
}