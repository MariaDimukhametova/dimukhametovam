package HomeWork2711;
public class Rectangle extends Shape{
    private int length;
    private int width;

    public Rectangle (int length, int width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public void getArea() {
        System.out.println("Rectangle's area: " + length * width);
    }
}
