
    //18.09, классная. Поменять местами максимальное и минимальное числа в массиве
    import java.util.Scanner;
    public class a3{
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int n = in.nextInt();
            int max = 0;
            int min = Integer.MAX_VALUE;
            int maxI = 0;
            int minI = 0;
            int array[] = new int[n];
            for (int i = 0; i < n; i++) {
                array[i] = in.nextInt();
            }
            for (int i = 0; i < n; i++) {
                if ( array[i] > max ) {
                    max = array[i];
                    maxI = i;
                }
            }
            for (int i = 0; i < n; i++) {
                if ( array[i] < min ) {
                    min = array[i];
                    minI = i;
                }
            }
            array[minI] = max;
            array[maxI] = min;
            for (int i = 0; i < n; i++) {
                System.out.println(array[i] + " ");
            }
        }
    }

