//Задача 4, сортировка массива
import java.util.Scanner;
import java.util.Random;

public class Zadacha4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        int a = in.nextInt();
        int b = in.nextInt();
        int[][] ar = new int[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                ar [i][j] = rand.nextInt(10);
            }
        }
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                System.out.print( ar[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("\n");

        int min = Integer.MAX_VALUE;
        int i1 = 0;
        int k = 0;
        int A;
        int i;
        while (k < (b - 1)) {
            for ( i = k; i < b ; i++) {
                if (ar[0][i] < min) {
                    i1 = i;
                    min = ar[0][i];
                }
            }
            for(i = 0 ; i < a; i++) {
                if (i == 0) {
                    A = ar [i][0 + k];
                    ar [i][0 + k]=min;
                    ar [i][i1] = A;
                }
                else {
                    A = ar [i][0 + k];
                    ar [i][0 + k]=ar [i][i1];
                    ar [i][i1] = A;
                }
            }
            k++;
            min = Integer.MAX_VALUE;
        }
        for ( i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                System.out.print(ar[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

