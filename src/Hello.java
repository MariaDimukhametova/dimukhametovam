//18.09, классная, найти максимальный чётный элемент массива целых чисел.
import java.util.Scanner;
public class Hello {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int max = 0;
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = in.nextInt();
        }
        for (int i = 0; i < n; i++) {
            if (array[i] % 2 == 0 && array[i] > max) {
                max = array[i];
            }
        }
        System.out.println(max);
    }
}