package HomeWork2011;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Admin admin = new Admin();
        ArrayList<Customer> customerArrayList = admin.createCustomersArrayListFromFile();
        ArrayList<String> linkArrayList = admin.assignLink();
        ArrayList<Order> orderArrayList = new ArrayList<>();
        admin.createOrdersFromFile(orderArrayList, linkArrayList);

        while (true) {

            System.out.println("Выберите действие:");
            System.out.println("1. Сделать заказ." + "\n" + "2. Вывести информацию о конкретном заказе." + "\n"
                    + "3. Вывести информацию обо всех заказах." + "\n" + "4. Вывести список покупателей.");

            int sc = scanner.nextInt();
            switch (sc) {
                case 1:
                    //новый заказ с консоли
                    admin.createNewOrder(orderArrayList, customerArrayList);
                    System.out.println("Ваш заказ успешно оформлен.");
                    break;
                case 2:
                    //вывод конкретного заказа
                    System.out.println("Введите имя покупателя:");
                    String name = scanner.next();
                    admin.checkOrder(name,orderArrayList);
                    break;
                case 3:
                    //вывод всех заказов
                    for (Order order : orderArrayList) {
                        order.getOrder();
                    }
                    break;
                case 4:
                    //вывод списка покупателей
                    for (Customer customer : customerArrayList) {
                    System.out.println(customer);
                    }
                    break;
                default:
                    System.out.println("Введите заново: ");
            }
        }
    }
}



